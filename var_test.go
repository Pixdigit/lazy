package lazy

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestResolving(t *testing.T) {

	test := 0.0

	p1 := Invariant(1)
	p2 := Invariant(2)
	p3 := Invariant(3)
	p4 := Invariant(4)
	p5 := PointerVar(&test)

	test = 1

	result := Div(Sub(Mul(Add(p1, p2), p3), p4), p5)
	result.Resolve()
	t.Log("Calculation result:", result.Value)
	tools.Test(result.Value == 5, "invalid result", t)
	tools.Test(result.HasDependency(p1), "No dependency detected", t)
	tools.Test(result.HasDependency(p2), "No dependency detected", t)
	tools.Test(result.HasDependency(p3), "No dependency detected", t)
	tools.Test(result.HasDependency(p4), "No dependency detected", t)
	tools.Test(result.HasDependency(p5), "No dependency detected", t)
	tools.Test(!result.HasDependency(Invariant(0)), "Dependency on independed node", t)
}
