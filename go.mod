module gitlab.com/Pixdigit/lazy

require (
	github.com/pkg/errors v0.8.1
	gitlab.com/Pixdigit/goTestTools v0.0.0-20180720152516-ad30e7e0c720
	gitlab.com/Pixdigit/uniqueID v1.0.0
)
